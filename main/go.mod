module github.com/Cyber4EDU/bbbDispatcher/main

go 1.14

require (
	github.com/antigloss/go v0.0.0-20200109080012-05d5d0918164
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.4
	github.com/spf13/viper v1.7.0

)
