package main

import (
	"crypto/sha1"
	"encoding/hex"
	"encoding/xml"
	"fmt"
	"github.com/Cyber4EDU/bbbDispatcher/main/model"
	"github.com/Cyber4EDU/bbbDispatcher/main/utils"
	"github.com/antigloss/go/logger"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/spf13/viper"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync"
	"time"
)

var apiPath = "/bigbluebutton/api/"
var secret string
var host string
var modsecret string

var useSSL bool
var certFile string
var keyFile string

var serverPort int
var maxCors int
var pollDelay time.Duration

var logLevel string
var logPath string
var logFilePrefix string
var logConsole bool

var meetings []*model.Meeting
var meetingsMux sync.RWMutex

var roomsMux sync.Map

const (
	modeModerator   = "moderator"
	modeParticipant = "participant"
)

func createRoom(
	roomName string,
	joinURL string,
	partMax uint32,
) *model.Response {

	endPoint := "create"
	var paramMap = make(map[string]string)
	var params []string
	//endPoint:="create"
	uniqueRoomName := fmt.Sprintf("%s - %s", roomName, uuid.New().String())

	logger.Info("createRoom: %s", uniqueRoomName)

	paramMap["name"] = uniqueRoomName
	paramMap["meetingID"] = uuid.New().String()
	paramMap["attendeePW"] = uuid.New().String()
	paramMap["moderatorPW"] = uuid.New().String()
	paramMap["welcome"] = "<h1>Herzlich Willkommen!</h1>" +
		"<p>Bitte haben einen Moment Geduld, weitere Teilnehmer*innen werden dem Raum zugewiesen.</p>" +
		fmt.Sprintf("<p>Dem Raum werden max %d Teilnehmer*innen zugeteilt.</p>", partMax) +
		fmt.Sprintf("<p>Falls es doch zu lange dauern sollte: <a href=%s>Raumzuteilung erneut starten ...</a></p><hr/>", joinURL)
	paramMap["maxParticipants"] = "20"
	paramMap["record"] = "false"
	paramMap["moderatorOnlyMessage"] = "<p>Liebe Moderator*in, bitte denke an das Sichern des Chats und der Notizen<p/>"
	paramMap["bannerText"] = "#schuleneudenken"
	paramMap["allowModsToUnmuteUsers"] = "true"

	for k, v := range paramMap {
		//logger.Info("k:", k, "v:", v)
		params = append(params, fmt.Sprintf("%s=%s", k, url.QueryEscape(v)))
	}

	paramString := strings.Join(params[:], "&")
	urlString := getURLWithChecksum(endPoint, paramString, apiPath)
	body := queryBbbAPI(urlString)
	if body == nil {
		return nil
	}

	var resp model.Response
	err := xml.Unmarshal(body, &resp)
	if err != nil {
		return nil
	}

	if resp.Returncode == "SUCCESS" {
		return &resp
	}

	logger.Error("could not create meeting: %s", resp.Message)
	return nil
}

func updateBbbMeetings() {
	params := fmt.Sprintf("random=%d", rand.Int())
	urlString := getURLWithChecksum("getMeetings", params, apiPath)
	body := queryBbbAPI(urlString)
	if body == nil {
		logger.Error("updateBbbMeetings: received empty response body")
		return
	}
	// Decode response body
	var resp model.Response
	if err := xml.Unmarshal(body, &resp); err != nil {
		logger.Error("updateBbbMeetings: %v", err)
		return
	}

	// Update meetings
	meetingsMux.Lock()
	meetings = resp.Meetings.Meetings
	meetingsMux.Unlock()
}

func reduceFunnyWords(usernames []string) []string {
	var reducedFw []string
	for _, w := range model.FunnyWords {
		if !utils.Contains(usernames, w) {
			reducedFw = append(reducedFw, w)
		}
	}
	return reducedFw
}

func joinBbbMeeting(
	w http.ResponseWriter,
	r *http.Request,
	meeting *model.Meeting,
	mode string,
) {
	// We mutate our meeting, so we need to protect it
	// from concurrenct access
	meeting.Mux.Lock()
	defer meeting.Mux.Unlock()

	endPoint := "join"
	var paramMap = make(map[string]string)
	var params []string

	logger.Info("joinBbbMeeting(%s): %s", mode, meeting.MeetingName)

	if mode == modeModerator {
		paramMap["fullName"] = "Moderator"
		paramMap["meetingID"] = meeting.MeetingID
		paramMap["password"] = meeting.ModeratorPW
	} else {
		currentUserNames := getUsernames(meeting)
		reduced := reduceFunnyWords(currentUserNames)
		rnd := rand.Intn(len(reduced) - 1)
		randomName := reduced[rnd]
		paramMap["fullName"] = randomName
		paramMap["meetingID"] = meeting.MeetingID
		paramMap["password"] = meeting.AttendeePW
	}

	for k, v := range paramMap {
		//logger.Info("k:", k, "v:", v)
		params = append(params, fmt.Sprintf("%s=%s", k, url.QueryEscape(v)))
	}

	// Register our joined user
	if mode == modeModerator {
		meeting.ModeratorCount++
	} else {
		meeting.ParticipantCount++
	}

	paramString := strings.Join(params[:], "&")
	urlString := getURLWithChecksum(endPoint, paramString, apiPath)
	redirect(w, r, urlString)
}

func getUsernames(meeting *model.Meeting) []string {
	var usernames []string

	for _, a := range meeting.Attendees.Attendees {
		usernames = append(usernames, a.FullName)
	}

	return usernames
}

func queryBbbAPI(urlString string) []byte {
	resp, err := http.Get(urlString)
	if err != nil {
		logger.Error("queryBbbAPI: HTTP error %v", err)
		return nil
	}
	// Check response
	if resp.StatusCode >= 300 {
		logger.Error("queryBbbAPI: error sending request to BBB: %v",
			resp.Status)
		return nil
	}

	// Load response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logger.Error("queryBbbAPI: coud not read: %v", err)
		return nil
	}

	return body
}

// Wouldn't a template be nice :/
func htmlEscape(str string) string {
	str = strings.ReplaceAll(str, "<", "&lt;")
	return strings.ReplaceAll(str, ">", "&gt;")
}

//noinspection ALL
func errorMessage(w http.ResponseWriter, r *http.Request, msg string) {
	//w.Header().Set("Content-Type", "application/html+xml; charset=utf-8")
	w.Header().Set("Content-Type", "text/html; charset=utf-8")

	fmt.Fprint(w, "<HTML><HEAD><TITLE>bbbDispatcher Error</TITLE>")
	fmt.Fprint(w, "<link rel='stylesheet' href='http://xkcd-embedder.fahmidur.us/css/xkcd-embedder.css'/>")
	fmt.Fprint(w, "</HEAD><BODY>")
	fmt.Fprint(w, "<h1>Error occurred!</h1>")
	fmt.Fprintf(w, "<p>%s</p>", htmlEscape(msg))
	fmt.Fprint(w, "<hr/><p><div class='xkcd-embed' data-id='random'></div></p>")
	fmt.Fprint(w, "<hr/><p><a href='https://digitalitaet20.de/'>Digitalität 2.0 Barcamp</a></p>")
	fmt.Fprint(w, "<script src='http://xkcd-embedder.fahmidur.us/js/xkcd-embedder.js'></script>")
	fmt.Fprint(w, "</BODY></HTML>")

}

func joinMeetingRest(w http.ResponseWriter, r *http.Request) {

	topic := mux.Vars(r)["topic"]
	partMax := mux.Vars(r)["partMax"]
	partMaxInt, err := strconv.ParseUint(partMax, 10, 16)
	if err != nil {
		logger.Error("joinMeetingRest:", err)
		errorMessage(w, r, err.Error())
		return
	}

	joinURL := fmt.Sprintf("https://%s%s", r.Host, r.URL.String())
	logger.Info("joinMeetingRest: %s / %s", topic, partMax)

	// Try to find a free meeting slot
	meeting := determineFreeMeeting(
		topic, modeParticipant, uint32(partMaxInt), joinURL)
	if meeting != nil {
		joinBbbMeeting(w, r, meeting, modeParticipant)
		return
	}

	// Inform the user that we failed.
	errorMessage(w, r, "Sorry, currently BBB cluster not available!")
}

func joinMeetingModRest(w http.ResponseWriter, r *http.Request) {
	topic := mux.Vars(r)["topic"]
	secret := mux.Vars(r)["secret"]

	logger.Info("joinMeetingModRest: %s", topic)

	// This is btw not safe against timing attacks.
	// Just saying.
	if secret != modsecret {
		errorMessage(w, r, "Access denied to join as moderator!")
		return
	}

	// Try to find a meeting slot
	meeting := determineFreeMeeting(topic, modeModerator, 0, "")
	if meeting != nil {
		joinBbbMeeting(w, r, meeting, modeModerator)
		return
	}

	// We tried everything we could, just inform the user
	// that we failed.
	errorMessage(w, r, "Sorry, our BBB cluster is currently not available!")
}

func determineFreeMeeting(
	topic string,
	partMode string,
	partMax uint32,
	joinURL string,
) *model.Meeting {
	// Joinin a room is locked if the topic is being created right now.
	// So, we await this to be finished
	for {
		_, locked := roomsMux.Load(topic)
		if !locked {
			break
		}
		time.Sleep(50 * time.Millisecond) // *sigh*
	}

	// Find meeting by topic
	var filteredMeeting *model.Meeting
	if partMode == modeModerator {
		filteredMeeting = filterModMeeting(topic, meetings, partMax)
	} else {
		filteredMeeting = filterMeeting(topic, meetings, partMax)
	}

	// In case we do not have a free meeting slow, create
	// an overflow room.
	if filteredMeeting == nil {
		// Lock topic
		roomsMux.Store(topic, true)
		response := createRoom(topic, joinURL, partMax)

		if response == nil {
			logger.Error("Room was not created.")
			// Release lock
			roomsMux.Delete(topic)
			return nil
		}

		// Refresh state from scalelite and try again
		updateBbbMeetings()
		roomsMux.Delete(topic)

		return determineFreeMeeting(topic, partMode, partMax, joinURL)
	}

	logger.Info("determineFreeMeeting: %s / %d", topic, partMax)
	return filteredMeeting
}

func getURLWithChecksum(
	endPoint string,
	params string,
	path string,
) string {
	checksum := endPoint + params + secret

	h := sha1.New()
	h.Write([]byte(checksum))

	sha1Hash := h.Sum(nil)
	sha1HashHex := hex.EncodeToString(sha1Hash)

	urlString := host + path + endPoint + "?" +
		params + "&checksum=" + sha1HashHex

	return urlString
}

func getState(meetings []*model.Meeting) []string {
	var bbbState []string

	for _, m := range meetings {
		var bbbStateLine []string
		//if m[i].Running == "true" {
		joinedVoice := 0
		hasVideo := 0
		isListeningOnly := 0
		isPresenter := 0
		isModerator := m.ModeratorCount
		running := m.Running

		bbbStateLine = append(bbbStateLine, m.MeetingName)

		bbbStateLine = append(bbbStateLine, "("+m.MeetingID+")")
		participantCount := fmt.Sprint(m.ParticipantCount)
		bbbStateLine = append(bbbStateLine, " - run: "+fmt.Sprint(running))
		bbbStateLine = append(bbbStateLine, " - pco: "+fmt.Sprint(
			participantCount))

		attendees := m.Attendees.Attendees
		for _, a := range attendees {
			if a.HasJoinedVoice == "true" {
				joinedVoice++
			}
			if a.HasVideo == "true" {
				hasVideo++
			}
			if a.IsListeningOnly == "true" {
				isListeningOnly++
			}
			if a.IsPresenter == "true" {
				isPresenter++
			}

		}

		bbbStateLine = append(bbbStateLine, " mod: "+fmt.Sprint(isModerator))
		bbbStateLine = append(bbbStateLine, " pre: "+fmt.Sprint(isPresenter))
		bbbStateLine = append(bbbStateLine, " vid: "+fmt.Sprint(hasVideo))
		bbbStateLine = append(bbbStateLine, " jvo: "+fmt.Sprint(joinedVoice))
		bbbStateLine = append(bbbStateLine, " lon: "+fmt.Sprint(isListeningOnly))

		bbbState = append(bbbState, strings.Join(bbbStateLine[:], ""))
	}
	return bbbState
}

func printState(meetings []*model.Meeting) {
	a := getState(meetings)
	for _, s := range a {
		logger.Info(s)
	}
}

func getXMLStatus(w http.ResponseWriter, r *http.Request) {
	secret := mux.Vars(r)["secret"]

	if secret != modsecret {
		errorMessage(w, r, "access denied")
		return
	}

	w.Header().Set("Content-Type", "application/xml; charset=utf-8")
	buf, err := xml.MarshalIndent(meetings, "", "  ")
	if err == nil {
		//noinspection GoUnhandledErrorResult
		fmt.Fprintf(w, string(buf))
	}
}

func getHTMLStatus(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	fmt.Fprintf(w, "<pre>v. 0.1.0</pre>")
	if meetings == nil {
		fmt.Fprintf(w, "<h1>no data</h1>")
	} else {
		fmt.Fprintf(w, "<pre>")
		a := getState(meetings)
		for _, s := range a {
			fmt.Fprintf(w, "%s<br/>", s)
		}
		fmt.Fprintf(w, "</pre>")
	}

	fmt.Fprintf(w, "<script>setTimeout(function () {location.reload();}, 2000);</script>")
}

func redirect(w http.ResponseWriter, r *http.Request, url string) {
	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}

func testRedirect(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "https://bbb.cyber4edu.org/b/der-xh3-hnn", http.StatusTemporaryRedirect)
}

func filterMeeting(
	topic string,
	meetings []*model.Meeting,
	partMax uint32,
) *model.Meeting {

	meetingsMux.RLock()
	defer meetingsMux.RUnlock()

	// Find meeting by name and free slot
	for _, m := range meetings {
		if strings.HasPrefix(m.MeetingName, topic) &&
			m.ParticipantCount < partMax {
			return m
		}
	}

	// No meeting.
	return nil
}

func filterModMeeting(
	topic string,
	meetings []*model.Meeting,
	partMax uint32,
) *model.Meeting {

	meetingsMux.RLock()
	defer meetingsMux.RUnlock()

	for _, m := range meetings {
		if strings.HasPrefix(m.MeetingName, topic) &&
			m.ModeratorCount == 0 {
			return m
		}
	}

	return nil
}

func updateMeetingsDataLoop() {
	logger.Info("updateMeetingsDataLoop")
	updateBbbMeetings()
}

func pollMeetingsDataLoop() {
	logger.Info("start polling")
	for {
		updateBbbMeetings()
		time.Sleep(pollDelay * time.Millisecond)
	}
}

func printDataLoop() {
	logger.Info("printDataLoop")
	if meetings != nil {
		printState(meetings)
	}
}

func init() {
	rand.Seed(time.Now().UnixNano())

	viper.SetConfigName("config")
	viper.AddConfigPath(".")

	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		panic(fmt.Sprintf("Fatal error config file: %v \n", err))
	}

	secret = viper.GetString("bbbApi.secret")
	host = viper.GetString("bbbApi.host")
	modsecret = viper.GetString("modsecret")

	useSSL = viper.GetBool("https.usessl")
	certFile = viper.GetString("https.certfile")
	keyFile = viper.GetString("https.keyfile")

	serverPort = viper.GetInt("serverport")
	maxCors = viper.GetInt("maxcors")
	pollDelay = time.Duration(viper.GetInt("polldelay"))

	logLevel = strings.ToLower(viper.GetString("logging.level"))
	logPath = viper.GetString("logging.path")
	logFilePrefix = viper.GetString("logging.fileprefix")
	logConsole = viper.GetBool("logging.console")
}

func main() {

	errl := logger.Init(logPath, 10, 2, 200, false)
	if errl != nil {
		panic("logging could not be initialized")
	}
	errlf := logger.SetFilenamePrefix(logFilePrefix, fmt.Sprintf("%s_current", logFilePrefix))
	if errlf != nil {
		panic("logfile could not be initialized")
	}
	logger.SetLogToConsole(logConsole)
	logger.SetLogThrough(true)
	logger.SetLogFunctionName(true)
	logger.SetLogFilenameLineNum(true)

	//switch logLevel {
	//case "error":
	//	logger.SetLevel(logger.ErrorLevel)
	//case "warn":
	//	logger.SetLevel(logger.WarnLevel)
	//case "info":
	//	logger.SetLevel(logger.InfoLevel)
	//case "debug":
	//	logger.SetLevel(logger.DebugLevel)
	//case "trace":
	//	logger.SetLevel(logger.TraceLevel)
	//default:
	//	logger.SetLevel(logger.DebugLevel)
	//}

	//createRoom(fmt.Sprintf("keks - %s", uuid.New().String()))
	//scheduler.Every(1).Seconds().Run(updateMeetingsDataLoop)
	go pollMeetingsDataLoop()
	//scheduler.Every(5).Seconds().Run(printDataLoop)

	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/status", getHTMLStatus).Methods("GET")
	router.HandleFunc("/rawstatus/{secret}", getXMLStatus).Methods("GET")
	router.HandleFunc("/redirect", testRedirect).Methods("GET")
	router.HandleFunc("/join/{topic}/{partMax}", joinMeetingRest).Methods("GET")
	router.HandleFunc("/joinMod/{topic}/{secret}", joinMeetingModRest).Methods("GET")

	var err error
	if useSSL {
		err = http.ListenAndServeTLS(fmt.Sprintf(":%d", serverPort), certFile, keyFile, router)
	} else {
		err = http.ListenAndServe(fmt.Sprintf(":%d", serverPort), router)
	}
	logger.Abort("service terminated: %s", err)
}
