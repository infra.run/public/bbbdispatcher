package model

import (
	"encoding/xml"
	"sync"
)

type Attendees struct {
	XMLName   xml.Name   `xml:"attendees"`
	Attendees []Attendee `xml:"attendee"`
}

type Attendee struct {
	XMLName         xml.Name `xml:"attendee"`
	UserID          string   `xml:"userID"`
	FullName        string   `xml:"fullName"`
	IsPresenter     string   `xml:"isPresenter"`
	IsListeningOnly string   `xml:"isListeningOnly"`
	HasJoinedVoice  string   `xml:"hasJoinedVoice"`
	HasVideo        string   `xml:"hasVideo"`
	ClientType      string   `xml:"clientType"`
}

type Metadata struct {
	XMLName             xml.Name `xml:"metadata"`
	BbbOriginVersion    string   `xml:"bbb-origin-version"`
	BbbOriginServerName string   `xml:"bbb-origin-server-name"`
	BbbOrigin           string   `xml:"bbb-origin"`
	GlListed            string   `xml:"gl-listed"`
}

type Meeting struct {
	XMLName               xml.Name  `xml:"meeting"`
	MeetingName           string    `xml:"meetingName"`
	MeetingID             string    `xml:"meetingID"`
	InternalMeetingID     string    `xml:"internalMeetingID"`
	CreateTime            uint64    `xml:"createTime"`
	CreateDate            string    `xml:"createDate"`
	VoiceBridge           string    `xml:"voiceBridge"`
	DialNumber            string    `xml:"dialNumber"`
	AttendeePW            string    `xml:"attendeePW"`
	ModeratorPW           string    `xml:"moderatorPW"`
	Running               string    `xml:"running"`
	Duration              int16     `xml:"duration"`
	Recording             string    `xml:"recording"`
	HasBeenForciblyEnded  string    `xml:"hasBeenForciblyEnded"`
	StartTime             uint64    `xml:"startTime"`
	EndTime               uint64    `xml:"endTime"`
	ParticipantCount      uint32    `xml:"participantCount"`
	ListenerCount         uint32    `xml:"listenerCount"`
	VoiceParticipantCount uint32    `xml:"voiceParticipantCount"`
	VideoCount            uint32    `xml:"videoCount"`
	MaxUsers              uint32    `xml:"maxUsers"`
	ModeratorCount        uint32    `xml:"moderatorCount"`
	Attendees             Attendees `xml:"attendees"`
	Metadata              Metadata  `xml:"metadata"`
	IsBreakout            string    `xml:"isBreakout"`

	Mux sync.Mutex
}

type Meetings struct {
	XMLName  xml.Name   `xml:"meetings"`
	Meetings []*Meeting `xml:"meeting"`
}

type Response struct {
	XMLName              xml.Name `xml:"response"`
	Returncode           string   `xml:"returncode"`
	Meetings             Meetings `xml:"meetings"`
	MeetingID            string   `xml:"meetingID"`
	InternalMeetingID    string   `xml:"internalMeetingID"`
	ParentMeetingID      string   `xml:"parentMeetingID"`
	AttendeePW           string   `xml:"attendeePW"`
	ModeratorPW          string   `xml:"moderatorPW"`
	CreateTime           string   `xml:"createTime"`
	VoiceBridge          string   `xml:"voiceBridge"`
	DialNumber           string   `xml:"dialNumber"`
	CreateDate           string   `xml:"createDate"`
	HasUserJoined        string   `xml:"hasUserJoined"`
	Duration             string   `xml:"duration"`
	HasBeenForciblyEnded string   `xml:"hasBeenForciblyEnded"`
	MessageKey           string   `xml:"messageKey"`
	Message              string   `xml:"message"`
	AuthToken            string   `xml:"auth_token"`
	SessionToken         string   `xml:"session_token"`
	Url                  string   `xml:"url"`
}
