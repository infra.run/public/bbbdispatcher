# bbbDispatcher

# URLs

## current state overview

  http://localhost:7080/status 

| Parameter | Beschreibung |
|---|---|
| run | Aktivität des Raums |
| pco | Anzahl der Teilnehmer*innen, inkl. der Moderator*in |
| pre | Anzahl der Presentator*innen |
| pre | Anzahl der Videostreams |
| jvo | Anzahl der Microphone |
| lon | Anzahl der Zuhörer |

  ChaosTreffchen(4b59b46f2e8e94fc218f4b9c36080e964a88e6e6) - run: true - pco: 1 mod: 1 pre: 1 vid: 0 jvo: 1 lon: 0

  Hund - 4506b278-aced-4428-a6d6-77847162f0cc(c0dccf4d-bdf1-4d56-942c-0fb9f28d83ac) - run: true - pco: 2 mod: 1 pre: 1 vid: 0 jvo: 2 lon: 0n 01/10

## join as participant 

  http://localhost:7080/join/TOPIC/MAXPARTICIPANTS

| Parameter | Beschreibung | 
|---|---|
|TOPIC | Sessionname, dieser wird um einen Zufallswert ergänzt damit die Name für alle Räumer einer Sesion eindeutig sind
| MAXPARTICIPANTS| maximale Anzahl von Teilnehmer*innen in einem Raum |

e.g. 

  http://localhost:7080/join/Sessio
  
## join as moderator

  http://localhost:7080/joinMod/TOPIC/SECRET

| Parameter | Beschreibung | 
|---|---|
|TOPIC | Sessionname, dieser wird um einen Zufallswert ergänzt damit die Name für alle Räumer einer Sesion eindeutig sind
| SECRET| Ein Geheimnis, ohne dem Moderator*innen keinen Zugang erhalten |

e.g.

  http://localhost:7080/joinMod/Session 01/krkejrhkwjrhqehrqkwjehrkqwerhkjwerhwqk 

